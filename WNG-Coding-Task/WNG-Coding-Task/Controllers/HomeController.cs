﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WNG_Coding_Task.Models;
using WNG_Coding_Task.Calculation;

namespace WNG_Coding_Task.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICalculationAppService _calculationAppService;

        public HomeController(ICalculationAppService calculationAppservice)
        {
            _calculationAppService = calculationAppservice;
        }

        public IActionResult Index(string numberInput)
        {
            try
            {
                if (HttpContext.Request.Method == "POST")
                { 
                    var output = _calculationAppService.CalculateAndReturnNumericSequences(numberInput);
                    return View("index", output);
                }
            }
            catch(Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View("index");
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo(assemblyName: "WNG_Coding_Task_Test")]

namespace WNG_Coding_Task.Models.Calculation
{
    internal class CalculationValidator : ICalculationValidator
    {
        public void ValidateInputNumber(string input)
        {
            if(string.IsNullOrWhiteSpace(input))
            {
                throw new Exception("Input cannot be Null or whitespace");
            }

            int parsedInput;

            bool result = int.TryParse(input, out parsedInput);

            if(!result)
            {
                throw new Exception("Input must be a whole number");
            }

            if(parsedInput < 1)
            {
                throw new Exception("Input must not be a negative number or 0");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WNG_Coding_Task.Models;
using WNG_Coding_Task.Models.Calculation.Dto;

namespace WNG_Coding_Task.Calculation
{
    public class CalculationAppService : ICalculationAppService
    {
        private readonly ICalculationDomainService _calculationDomainService;
        private readonly ICalculationValidator _calculationValidator;

        public CalculationAppService(ICalculationDomainService calculationDomainService,
                                     ICalculationValidator calculationValidator)
        {
            _calculationDomainService = calculationDomainService;
            _calculationValidator = calculationValidator;
        }

        public CalculationDto CalculateAndReturnNumericSequences(string input)
        {
            _calculationValidator.ValidateInputNumber(input);

            try
            {
                int parsedInput = int.Parse(input);

                var allNumbers = _calculationDomainService.AllNumbersUpToAndIncludingInput(parsedInput);
                var allOdd = _calculationDomainService.AllOddNumbersUpToAndIncludingInput(parsedInput);
                var allEven = _calculationDomainService.AllEvenNumbersUpToAndIncludingInput(parsedInput);
                var allAlphaReplace = _calculationDomainService.AllNumbersWithAlphaReplacements(parsedInput);

                CalculationDto calculationResults = new CalculationDto
                {
                    AllNumbersUpToInput = allNumbers,
                    AllOddNumbers = allOdd,
                    AllEvenNumbers = allEven,
                    AllNumbersAlphaReplace = allAlphaReplace
                };
                
                return calculationResults;
            }
            catch
            {
                throw new Exception("Something went wrong!");
            }
        }
    }
}

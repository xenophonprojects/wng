﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WNG_Coding_Task.Models.Calculation.Dto 
{
    public class CalculationDto
    {
        public CalculationDto()
        {

        }

        public IList<int> AllNumbersUpToInput { get; set; }
        public IList<int> AllOddNumbers { get; set; }
        public IList<int> AllEvenNumbers { get; set; }
        public IList<string> AllNumbersAlphaReplace { get; set; }
    }
}

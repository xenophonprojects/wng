﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo(assemblyName: "WNG_Coding_Task_Test")]

namespace WNG_Coding_Task.Models.Calculation
{
    internal class CalculationDomainService : ICalculationDomainService
    {
        public CalculationDomainService()
        {

        }

        //All numbers up to and including the number entered
        public IList<int> AllNumbersUpToAndIncludingInput(int input)
        {
            var numbers = new List<int>();

            for (int i = 0; i <= input; i++)
            {
                numbers.Add(i);
            }

            return numbers;
        }

        //All odd numbers up to and including the number
        public IList<int> AllOddNumbersUpToAndIncludingInput(int input)
        {
            var oddNumbers = new List<int>();

            for (int i = 1; i <= input; i++)
            {
                if (!IsNumberMultipleOf(i, 2) || i == input)
                {
                    oddNumbers.Add(i);
                }
            }

            return oddNumbers;
        }

        //All even numbers up to and including the number
        public IList<int> AllEvenNumbersUpToAndIncludingInput(int input)
        {
            var evenNumbers = new List<int>();

            for (int i = 1; i <= input; i++)
            {
                if (IsNumberMultipleOf(i, 2) || i == input)
                {
                    evenNumbers.Add(i);
                }
            }

            return evenNumbers;
        }

        /* 
         * All numbers up to and includingthe number entered,
         * EXCEPT when a number is a multiple of 3 output "C",
         * AND when a number is a multiple of 5 output "E",
         * AND when a number is a multiple of both 3 and 5 output Z
         */

        public IList<string> AllNumbersWithAlphaReplacements(int input)
        {
            var returnList = new List<string>();

            for (int i = 0; i <= input; i++)
            {
                if (IsNumberMultipleOf(i, 3))
                {
                    if (IsNumberMultipleOf(i, 5))
                    {
                        returnList.Add("Z");
                    }
                    else
                    {
                        returnList.Add("C");
                    }
                }
                else if (IsNumberMultipleOf(i, 5))
                {
                    returnList.Add("E");
                }
                else
                {
                    returnList.Add(i.ToString());
                }
            }

            return returnList;
        }

        /*
         * Helper Methods
         */

        private bool IsNumberMultipleOf(int number, int multiple)
        {
            if (number % multiple == 0)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using System.Collections.Generic;

namespace WNG_Coding_Task.Models
{
    public interface ICalculationDomainService
    {
        IList<int> AllNumbersUpToAndIncludingInput(int input);
        IList<int> AllOddNumbersUpToAndIncludingInput(int input);
        IList<int> AllEvenNumbersUpToAndIncludingInput(int input);
        IList<string> AllNumbersWithAlphaReplacements(int input);
    }
}

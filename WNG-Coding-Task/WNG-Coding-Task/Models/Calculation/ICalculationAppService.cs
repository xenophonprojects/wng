﻿using WNG_Coding_Task.Models.Calculation.Dto;

namespace WNG_Coding_Task.Calculation
{
    public interface ICalculationAppService
    {
        CalculationDto CalculateAndReturnNumericSequences(string input);
    }
}

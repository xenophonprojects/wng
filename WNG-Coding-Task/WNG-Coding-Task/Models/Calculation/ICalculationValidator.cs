﻿namespace WNG_Coding_Task.Models
{
    public interface ICalculationValidator
    {
        void ValidateInputNumber(string input);
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WNG_Coding_Task.Models.Calculation;

namespace WNG_Coding_Task_Test
{
    [TestClass]
    public class CalculationValidatorTests
    {
        [TestMethod]
        public void ValidateInputNumber_ValidInput_Success()
        {
            //Arrange
            CalculationValidator validator = new CalculationValidator();

            //Act & Assert
            validator.ValidateInputNumber("10");
        }

        [TestMethod]
        public void ValidateInputNumber_InvalidInput_ExceptionThrown()
        {
            //Arrange
            CalculationValidator validator = new CalculationValidator();

            //Act & Assert
            Assert.ThrowsException<Exception>(() => validator.ValidateInputNumber("text"));
        }
    }
}

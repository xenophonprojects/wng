﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WNG_Coding_Task.Calculation;
using WNG_Coding_Task.Models;

namespace WNG_Coding_Task_Test
{
    [TestClass]
    public class CalculationAppServiceTests
    {
        [TestMethod]
        public void CalculateAndReturnNumericSequences_ValidInput_Success()
        {
            //Arrange
            var calculationDomainService = new Mock<ICalculationDomainService>();
            var calculationValidator = new Mock<ICalculationValidator>();

            var calculationAppService = new CalculationAppService(calculationDomainService.Object,
                                                                  calculationValidator.Object);

            calculationValidator.Setup(c => c.ValidateInputNumber(1.ToString()));

            calculationDomainService.Setup(c => c.AllNumbersUpToAndIncludingInput(5))
                                    .Returns(new List<int> { 0, 1, 2, 3, 4, 5 });

            calculationDomainService.Setup(c => c.AllOddNumbersUpToAndIncludingInput(5))
                                    .Returns(new List<int> { 1, 3, 5 });

            calculationDomainService.Setup(c => c.AllEvenNumbersUpToAndIncludingInput(5))
                                    .Returns(new List<int> { 2, 4, 5 });

            calculationDomainService.Setup(c => c.AllNumbersWithAlphaReplacements(5))
                                    .Returns(new List<string> { "Z", "1", "2", "C", "4", "E" });

            //Act
            var result = calculationAppService.CalculateAndReturnNumericSequences("5");

            //Assert
            Assert.IsNotNull(result);

            CollectionAssert.AreEqual(result.AllNumbersUpToInput.ToList(), new List<int> { 0, 1, 2, 3, 4, 5 });
            CollectionAssert.AreEqual(result.AllEvenNumbers.ToList(), new List<int> { 2, 4, 5 });
            CollectionAssert.AreEqual(result.AllOddNumbers.ToList(), new List<int> { 1, 3, 5 });
            CollectionAssert.AreEqual(result.AllNumbersAlphaReplace.ToList(), new List<string> { "Z", "1", "2", "C", "4", "E" });
        }

        [TestMethod]
        public void CalculateAndReturnNumericSequences_ErrorThrown_Fail()
        {
            //Arrange
            var calculationDomainService = new Mock<ICalculationDomainService>();
            var calculationValidator = new Mock<ICalculationValidator>();

            var calculationAppService = new CalculationAppService(calculationDomainService.Object,
                                                                  calculationValidator.Object);

            calculationValidator.Setup(c => c.ValidateInputNumber(1.ToString()));

            calculationDomainService.Setup(c => c.AllNumbersUpToAndIncludingInput(5))
                                    .Throws(new Exception());

            calculationDomainService.Setup(c => c.AllOddNumbersUpToAndIncludingInput(5))
                                    .Returns(new List<int> { 1, 3, 5 });

            calculationDomainService.Setup(c => c.AllEvenNumbersUpToAndIncludingInput(5))
                                    .Returns(new List<int> { 2, 4, 5 });

            calculationDomainService.Setup(c => c.AllNumbersWithAlphaReplacements(5))
                                    .Returns(new List<string> { "Z", "1", "2", "C", "4", "E" });

            //Act & Assert
            Assert.ThrowsException<Exception>(() => calculationAppService.CalculateAndReturnNumericSequences("5"));
        }
    }
}

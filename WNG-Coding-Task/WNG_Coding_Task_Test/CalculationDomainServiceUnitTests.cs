﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WNG_Coding_Task.Models;
using WNG_Coding_Task.Models.Calculation;

namespace WNG_Coding_Task_Test
{
    [TestClass]
    public class CalculationDomainServiceUnitTests
    {
        /*
         * AllNumbersUpToAndIncludingInput Tests
         */

        [TestMethod]
        public void AllNumbersUpToAndIncludingInput_PositiveIntInput_Success()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllNumbersUpToAndIncludingInput(5);

            //Assert
            CollectionAssert.AreEqual(result as List<int>, new List<int> { 0, 1, 2, 3, 4, 5 });
        }

        [TestMethod]
        public void AllNumbersUpToAndIncludingInput_NegativeIntInput_ReturnsEmptyList()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllNumbersUpToAndIncludingInput(-1);

            //Assert
            Assert.AreEqual(result.Count, 0);
        }

        /*
         * AllOddNumbersUpToAndIncludingInput Tests
         */

        [TestMethod]
        public void AllEvenNumbersUpToAndIncludingInput_PositiveIntInput_Success()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllEvenNumbersUpToAndIncludingInput(5);

            //Assert
            CollectionAssert.AreEqual(result as List<int>, new List<int> { 2, 4, 5 });
        }

        [TestMethod]
        public void AllEvenNumbersUpToAndIncludingInput_NegativeIntInput_ReturnsEmptyList()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllEvenNumbersUpToAndIncludingInput(-1);

            //Assert
            Assert.AreEqual(result.Count, 0);
        }

        /*
         * AllEvenNumbersUpToAndIncludingInput Tests
         */

        [TestMethod]
        public void AllOddNumbersUpToAndIncludingInput_PositiveIntInput_Success()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllOddNumbersUpToAndIncludingInput(5);

            //Assert
            CollectionAssert.AreEqual(result as List<int>, new List<int> { 1, 3, 5 });
        }

        [TestMethod]
        public void AllOddNumbersUpToAndIncludingInput_NegativeIntInput_ReturnsEmptyList()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllOddNumbersUpToAndIncludingInput(-1);

            //Assert
            Assert.AreEqual(result.Count, 0);
        }

        /*
         * AllNumbersWithAlphaReplacements Tests
         */

        [TestMethod]
        public void AllNumbersWithAlphaReplacements_PositiveIntInput_Success()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllNumbersWithAlphaReplacements(5);

            //Assert
            CollectionAssert.AreEqual(result as List<string>, new List<string> { "Z", "1", "2", "C", "4", "E" });
        }

        [TestMethod]
        public void AllNumbersWithAlphaReplacements_NegativeIntInput_ReturnsEmptyList()
        {
            //Arrange
            CalculationDomainService calculationDomain = new CalculationDomainService();

            //Act
            var result = calculationDomain.AllNumbersWithAlphaReplacements(-1);

            //Assert
            Assert.AreEqual(result.Count, 0);
        }
    }
}
